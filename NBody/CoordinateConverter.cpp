#include "CoordinateConverter.h"

CoordinateConverter::CoordinateConverter() : _toViewFactor(0.0f), _toWorldFactor(0.0f)
{
	_viewCenter = { 0, 0 };
	_worldCenter = DirectX::XMVectorZero();
}

void CoordinateConverter::Update(POINT& viewCenter, DirectX::XMVECTOR& worldCenter, float maxWorldDistance)
{
	_viewCenter = viewCenter;
	_worldCenter = worldCenter;
	//Set converter factor
	_toViewFactor = min(_viewCenter.x, _viewCenter.y) / maxWorldDistance;
	_toWorldFactor = 1.0f / _toViewFactor;
}

int CoordinateConverter::ConverterToView(float worldPoint, float worldCenter, int viewCenter, bool invert)
{
	return viewCenter + (int)((invert ? -1 : 1)*_toViewFactor*(worldPoint - worldCenter));
}

float CoordinateConverter::ConverterToWorld(int viewPoint, int viewCenter, float worldCenter, bool invert)
{
	return worldCenter + (invert ? -1.0f : 1.0f)*_toWorldFactor*(viewPoint - viewCenter);
}

int CoordinateConverter::ConvertLengthToView(float length)
{
	return ConverterToView(length, 0.0f, 0, false);
}

void CoordinateConverter::ConvertWorldToView(const DirectX::XMVECTOR& v, POINT& p)
{
	p =
	{
		ConverterToView(DirectX::XMVectorGetX(v), DirectX::XMVectorGetX(_worldCenter), _viewCenter.x, false),
		ConverterToView(DirectX::XMVectorGetY(v), DirectX::XMVectorGetY(_worldCenter), _viewCenter.y, true)
	};
}

void CoordinateConverter::ConvertViewToWorld(const POINT& p, DirectX::XMVECTOR& v)
{
	v = DirectX::XMVectorSet(
		ConverterToWorld(p.x, _viewCenter.x, DirectX::XMVectorGetX(_worldCenter), false),
		ConverterToWorld(p.y, _viewCenter.y, DirectX::XMVectorGetY(_worldCenter), true),
		0.0f,
		0.0f);
}