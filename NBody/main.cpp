// NBody.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "NBody.h"
#include <windowsx.h>

#define MAX_LOADSTRING 100

#define WM_IDLE			WM_USER + 1

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int, HWND&);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

	HWND hWnd = 0L;

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_NBODY, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow, hWnd))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_NBODY));

    MSG msg;

    // Main message loop:
	while (true)
	{
		if (::PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				break;
			}
			else if (!::TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
			{
				::TranslateMessage(&msg);
				::DispatchMessage(&msg);
			}
		}
		else
		{
			//Idle
			::PostMessage(hWnd, WM_IDLE, 0L, 0L);
		}
	}


    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_NBODY));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_NBODY);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow, HWND& hWnd)
{
   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, 400, 400, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//

#include "Data.h"
#include "Processor.h"
#include "View.h"
#include "BackBuffer.h"

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static Processor processor;
	static Data data;
	static View view;
	static BackBuffer bb;
	static bool update = true;

    switch (message)
    {
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
            case IDM_ABOUT:
                ::DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                ::DestroyWindow(hWnd);
                break;
            default:
                return ::DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
	case WM_KEYUP:
	{
		auto shift = ((::GetKeyState(VK_SHIFT) & 0x8000) == 0x8000);
		auto control = ((::GetKeyState(VK_CONTROL) & 0x8000) == 0x8000);
		std::function<void(void)> f;
		switch (wParam)
		{
		case VK_BACK:
			view.ResetDistance();
			break;
		case VK_OEM_PLUS:
			f = [&]() { view.DecreaseDistance(); };
			break;
		case VK_OEM_MINUS:
			f = [&]() { view.IncreaseDistance(); };
			break;
		case VK_LEFT:
			f = [&]() { processor.DecreaseStep(); };
			break;
		case VK_RIGHT:
			f = [&]() { processor.IncreaseStep(); };
			break;
		case VK_SPACE:
			update = !update;
			break;
		case '8':
			data.ToggleA();
			break;
		case '9':
			f = [&]() { data.DecreaseFX(); };
			break;
		case '0':
			f = [&]() { data.IncreaseFX(); };
			break;
		case 'a':
		case 'A':
			processor.ToggleAcceleration();
			break;
		case 'c':
		case 'C':
			view.ToggleClearCanvas();
			break;
		case 'd':
		case 'D':
			f = [&]() { data.DeleteBody(); };
			break;
		case 'g':
		case 'G':
			view.ToggleGravitationalInteraction();
			break;
		case 'k':
		case 'K':
			data.KillVelocity();
			break;
		case 'm':
		case 'M':
			f = [&]() { data.CreateNewBody(1, control, view.GetMaxDistance()); };
			break;
		case 'n':
		case 'N':
			f = [&]() { data.CreateNewBody(0, control, view.GetMaxDistance()); };
			break;
		case 't':
		case 'T':
			view.ToggleTrail();
			break;
		}
		//Testing shift
		if (f)
		{
			for (int i = 0; i < (shift ? 10 : 1); i++)
			{
				f();
			}
		}
		break;
	}
	case WM_LBUTTONDOWN:
	{
		POINT p = { GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam) };
		data.CreateNewBody(p, view.GetCoordinateConverter(), (wParam & MK_SHIFT) == MK_SHIFT, (wParam & MK_CONTROL) == MK_CONTROL);
		break;
	}
	case WM_MOUSEWHEEL:
	{
		auto zDelta = GET_WHEEL_DELTA_WPARAM(wParam);
		if (zDelta > 0) {
			view.DecreaseDistance();
		}
		else {
			view.IncreaseDistance();			
		}
		break;
	}
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = ::BeginPaint(hWnd, &ps);
			bb.Initialize(hdc, hWnd);
			view.Render(bb.GetDC(), bb.GetWidth(), bb.GetHeight(), data);
			bb.BitBlt(hdc);
            ::EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
		processor.Finalize();
		data.Finalize();
		view.Finalize();
		bb.Finalize(hWnd);
        ::PostQuitMessage(0);
        break;
	case WM_IDLE:
		if (update)
		{
			processor.DoTurn(data);
		}
		::InvalidateRect(hWnd, nullptr, FALSE);
		break;
	case WM_CREATE:
		data.Initialize();
		view.Initialize();
		processor.Initialize();
		break;
	case WM_SIZE:
		bb.Finalize(hWnd);
		break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
