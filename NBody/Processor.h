#pragma once
#include "Data.h"

class Processor
{
private:
	float		_step;
	bool		_calculateAcceleration;
public:
	Processor();
	~Processor();
	//Initiliazing/Finalizing
	bool Initialize();
	bool Finalize();
	//Process
	bool DoTurn(Data& d);
	//Toggle
	void ToggleAcceleration() { _calculateAcceleration = !_calculateAcceleration; }
	void IncreaseStep() { _step *= 10.0f; }
	void DecreaseStep() { _step /= 10.0f; }
};

