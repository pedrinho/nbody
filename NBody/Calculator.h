#pragma once
#include "DataTypes.h"


class Calculator
{
public:
	//Operations
	static float GetRadius(float mass);
	static DirectX::XMVECTOR GetForce(Body& b1, Body& b2);
	static void CalculateOrbit(Body& center, Body& newBody, float x, float y, float mass, float cw);
};

