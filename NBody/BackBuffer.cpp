#include "BackBuffer.h"

BackBuffer::BackBuffer():_initialized(false), _hdc(0L), _hBmp(0L)
{
	::ZeroMemory(&_rc, sizeof(_rc));
}

BackBuffer::~BackBuffer()
{
}

bool BackBuffer::Initialize(HDC hdc, HWND hWnd)
{
	if (!_initialized)
	{
		::GetClientRect(hWnd, &_rc);
		_hdc = ::CreateCompatibleDC(hdc);
		_hBmp = ::CreateCompatibleBitmap(hdc, GetWidth(), GetHeight());
		::SelectObject(_hdc, _hBmp);
		_initialized = true;
	}

	return false;
}

bool BackBuffer::Finalize(HWND hWnd)
{
	if (_hdc != 0L)
	{
		::ReleaseDC(hWnd, _hdc);
		_hdc = 0L;
	}

	if (_hBmp != 0L)
	{
		::DeleteObject(_hBmp);
		_hBmp = 0L;
	}
	_initialized = false;
	::ZeroMemory(&_rc, sizeof(_rc));

	return false;
}

bool BackBuffer::BitBlt(HDC hdc)
{
	return (::BitBlt(hdc, _rc.left, _rc.top, GetWidth(), GetHeight(), _hdc, 0, 0, SRCCOPY) == TRUE);
}
