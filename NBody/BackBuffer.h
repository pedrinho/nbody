#pragma once
#include <Windows.h>

class BackBuffer
{
private:
	bool _initialized;
	HDC _hdc;
	HBITMAP _hBmp;
	RECT _rc;
public:
	BackBuffer();
	~BackBuffer();
	//Operations
	bool Initialize(HDC hDC, HWND hWnd);
	bool Finalize(HWND hWnd);
	bool BitBlt(HDC hdc);
	//Gets
	HDC GetDC() { return _hdc; }
	int GetWidth() { return _rc.right - _rc.left; }
	int GetHeight() { return _rc.bottom - _rc.top; }
};

