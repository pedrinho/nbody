#include "stdafx.h"
#include "View.h"
#include <math.h>
#include <sstream>
#include "stdafx.h"
#include <algorithm>
#include <iomanip>
#include "Calculator.h"

View::View() : 
	_clearCanvas(true),
	_trail(false),
	_gravitationalInteraction(false),
	_maxDistance(0.0f),
	_cc()
{
}

View::~View()
{
}

bool View::Initialize()
{
	return false;
}

bool View::Finalize()
{
	return false;
}

void View::ClearCanvas(HDC hdc, int width, int height)
{
	RECT canvas = { 0, 0, width, height };
	::FillRect(hdc, &canvas, (HBRUSH)::GetStockObject(BLACK_BRUSH));

}

void View::DrawAuxilliary(HDC hdc)
{
	//Creating gdi objects
	auto pen = ::CreatePen(PS_DOT, 1, RGB(25, 25, 25));
	auto oldPen = ::SelectObject(hdc, pen);
	auto oldBrush = ::SelectObject(hdc, ::GetStockObject(HOLLOW_BRUSH));
	auto factor = (int)::log10f(_maxDistance);
	for (int i = factor; i > (factor <= 2 ? 0 : factor - 2); i--)
	{
		auto radius = _cc.ConvertLengthToView(::powf(10.0f, (float)i));
		POINT p;
		_cc.ConvertWorldToView(_cc.GetWorldCenter(), p);
		::Ellipse(hdc, p.x - radius, p.y - radius, p.x + radius, p.y + radius);
	}
	::SelectObject(hdc, oldPen);
	::SelectObject(hdc, oldBrush);
	//Clearing gdi objects
	::DeleteObject(pen);
}

void View::DrawCenterMarks(HDC hdc, Data& d)
{
	//Drawing geometric center
	auto centerPen = ::CreatePen(PS_SOLID, 2, RGB(100, 0, 0));
	auto centerBrush = ::CreateSolidBrush(RGB(100, 0, 0));
	auto oldPen = ::SelectObject(hdc, centerPen);
	auto oldBrush = ::SelectObject(hdc, centerBrush);
	auto size = 2;
	POINT p;
	_cc.ConvertWorldToView(d.GetCenter(), p);
	::Ellipse(hdc, p.x - size, p.y - size, p.x + size, p.y + size);
	//Drawing center o mass
	auto comPen = ::CreatePen(PS_SOLID, 2, RGB(100, 100, 0));
	auto comBrush = ::CreateSolidBrush(RGB(100, 100, 0));
	::SelectObject(hdc, comPen);
	::SelectObject(hdc, comBrush);
	_cc.ConvertWorldToView(d.GetCoM(), p);
	::Ellipse(hdc, p.x - size, p.y - size, p.x + size, p.y + size);
	//Restoring objects
	::SelectObject(hdc, oldPen);
	::SelectObject(hdc, oldBrush);
	//Deleting objects
	::DeleteObject(centerPen);
	::DeleteObject(comPen);
	::DeleteObject(centerBrush);
	::DeleteObject(comBrush);
}

#define FP std::setprecision(2) << std::setiosflags(4096) << std::setw(9) << std::setfill(' ') 

void View::DrawLegend(HDC hdc, Data& d, int width, int height)
{
	//Setting text
	std::ostringstream oss;
	oss << "Total = " << d.GetTotalBodies() << std::endl
		<< "Center(" << FP << DirectX::XMVectorGetX(_cc.GetWorldCenter())
		<< "," << FP << DirectX::XMVectorGetY(_cc.GetWorldCenter())
		<< ")" << std::endl
		<< "CoM(" << FP << DirectX::XMVectorGetX(d.GetCoM())
		<< "," << FP << DirectX::XMVectorGetY(d.GetCoM())
		<< ")" << std::endl
		<< "MaxDistance = " << _maxDistance << std::endl
		<< "Total Mass = " << d.GetTotalMass();
	//Draw legend
	RECT rc = { 10 };
	rc.bottom = height;
	rc.right = width;
	::SetTextColor(hdc, RGB(200, 200, 200));
	::SetBkMode(hdc, TRANSPARENT);
	::DrawTextA(hdc, oss.str().c_str(), (int)oss.str().size(), &rc, DT_LEFT | DT_VCENTER);
}

void View::DrawBodies(HDC hdc, Data& d)
{
	auto pen = ::CreatePen(PS_SOLID, 1, RGB(200, 200, 200));
	auto brush = ::CreateSolidBrush(RGB(200, 200, 200));
	auto oldPen = ::SelectObject(hdc, pen);
	auto oldBrush = ::SelectObject(hdc, brush);
	d.ForEachBodies([&](Body& b)
	{
		auto dist =
			DirectX::XMVectorGetX(
				DirectX::XMVector2Length(
					DirectX::XMVectorSubtract(b.pos, _cc.GetWorldCenter())));
		//if (dist < _maxDistance)
		{
			POINT p1;
			_cc.ConvertWorldToView(b.pos, p1);
			auto radius = (int)CLAMP_MIN(_cc.ConvertLengthToView(Calculator::GetRadius(b.mass)), 1.0f);
			//Drawing trail
			if (_trail)
			{
				auto tPen = ::CreatePen(PS_SOLID, 1, RGB(50, 50, 50));
				auto old = ::SelectObject(hdc, tPen);
				::MoveToEx(hdc, p1.x, p1.y, NULL);
				for (auto pos : b.trail.trails)
				{
					POINT p2;
					_cc.ConvertWorldToView(pos, p2);
					::LineTo(hdc, p2.x, p2.y);
				}
				::SelectObject(hdc, old);
				::DeleteObject(tPen);
			}
			if (_gravitationalInteraction && b.interactions.maxInteraction.GetOldValue().force > 0.0f)
			{
				auto gPen = ::CreatePen(PS_SOLID, 1, RGB(150, 0, 0));
				auto old = ::SelectObject(hdc, gPen);
				::MoveToEx(hdc, p1.x, p1.y, NULL);
				POINT p2;
				_cc.ConvertWorldToView(b.interactions.maxInteraction.GetOldValue().pos, p2);
				::LineTo(hdc, p2.x, p2.y);
				::SelectObject(hdc, old);
				::DeleteObject(gPen);
			}
			//Drawing body
			::Ellipse(hdc, p1.x - radius, p1.y - radius, p1.x + radius, p1.y + radius);
		}
	});
	//Selecting old objects
	::SelectObject(hdc, oldPen);
	::SelectObject(hdc, oldBrush);
	//Deleting objects
	::DeleteObject(pen);
	::DeleteObject(brush);

}

bool View::Render(HDC hdc, int width, int height, Data & d)
{
	//Setting view and world centers
	POINT viewCenter = { width / 2, height / 2 };
	DirectX::XMVECTOR worldCenter = d.GetCoM();
	//Setting world distance
	if (_maxDistance == 0.0f)
	{
		/*if (d.GetTotalBodies() == 0 || d.GetMaxCenterDistance() == 0.0f)
		{
			_maxDistance = 1000.0f;
		}
		else */if (d.GetTotalBodies() == 1)
		{
			_maxDistance = 10.0f*Calculator::GetRadius(d.GetTotalMass());
		}
		else
		{
			_maxDistance = 1.0f*d.GetMaxCoMDistance();
		}
	}
	//Updating coordinate converting tool
	_cc.Update(viewCenter, worldCenter, _maxDistance);
	//Cleaning canvas
	if (_clearCanvas)
		ClearCanvas(hdc, width, height);
	//Drawing auxilliary lines
	DrawAuxilliary(hdc);
	//Drawing bodies
	DrawBodies(hdc, d);
	//Drawing center and com
	DrawCenterMarks(hdc, d);
	//Legend
	DrawLegend(hdc, d, width, height);

	return false;
}