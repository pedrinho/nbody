// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

#define MIN(a,b)				(a < b ? a : b)
#define MAX(a,b)				(a > b ? a : b)
#define CLAMP_MIN_MAX(v, l, u)	MIN(u, MAX(v, l))
#define CLAMP_MIN(v, l)			MAX(v, l)