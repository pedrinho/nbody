#include "stdafx.h"
#include "Processor.h"

Processor::Processor() : _calculateAcceleration(true), _step(0.00001f)
{
}

Processor::~Processor()
{
}

bool Processor::Initialize()
{
	return false;
}

bool Processor::Finalize()
{
	return false;
}

bool Processor::DoTurn(Data & d)
{
	//Calculating accelerations / detecting collision
	d.InteractBodies([&](Body& b1, Body& b2)
	{
		auto colided = false;
		auto b2Next = DirectX::XMVectorAdd(b2.pos, DirectX::XMVectorScale(b2.vel, _step));
		auto minDist = DirectX::XMVectorGetX(DirectX::XMVector2LinePointDistance(b2.pos, b2Next, b1.pos));
		auto totalRadius = Calculator::GetRadius(b1.mass) + Calculator::GetRadius(b2.mass);
		if (::isnan(minDist) || minDist < totalRadius)
		{
			auto p1 = DirectX::XMVectorSubtract(b2.pos, b1.pos);
			auto d1 = DirectX::XMVectorGetX(DirectX::XMVector2Length(p1));
			if (d1 < totalRadius)
			{
				colided = true;
			}
			else
			{
				auto p2 = DirectX::XMVectorSubtract(b2Next, b1.pos);
				auto pd = DirectX::XMVectorSubtract(p2, p1);
				auto dot1 = DirectX::XMVectorGetX(DirectX::XMVector2Dot(p1, pd));
				auto dot2 = DirectX::XMVectorGetX(DirectX::XMVector2Dot(p2, pd));
				//Detecting collision
				auto s1 = (dot1 >= 0.0f);
				auto s2 = (dot2 >= 0.0f);
				colided = (s1 ^ s2);
			}
			if(colided)
			{
				if (b1.mass > b2.mass)
				{
					b1.mass += b2.mass;
					b2.deleted = true;
				}
				else
				{
					b2.mass += b1.mass;
					b1.deleted = true;
				}
			}
		}
		//Calculating accelerations
		if(!colided && _calculateAcceleration)
		{
			auto force = Calculator::GetForce(b1, b2);
			if (b2.mass > 0.0f)
				b1.acc = DirectX::XMVectorAdd(b1.acc, DirectX::XMVectorScale(force, 1 / CLAMP_MIN(b1.mass, 1.0f)));
			if (b1.mass > 0.0f)
				b2.acc = DirectX::XMVectorSubtract(b2.acc, DirectX::XMVectorScale(force, 1 / CLAMP_MIN(b2.mass, 1.0f)));
			//Testing gravitational interaction
			auto f = DirectX::XMVectorGetX(DirectX::XMVector2Length(force));
			auto& b = (b1.mass > b2.mass ? b2 : b1);
			auto& pos = (b1.mass > b2.mass ? b1.pos : b2.pos);
			if (f > b.interactions.maxInteraction->force)
			{
				b.interactions.maxInteraction->force = f;
				b.interactions.maxInteraction->pos = pos;
			}
		}
	});
	//Calculating velocity, position, center and center of mass
	float totalMass = 0.0f;
	DirectX::XMVECTOR coM = DirectX::XMVectorZero();;
	DirectX::XMVECTOR center = DirectX::XMVectorZero();;
	//Browsing bodies
	d.ForEachBodies([&](Body& b)
	{
		//Setting cinamatic values
		b.vel = DirectX::XMVectorAdd(b.vel, DirectX::XMVectorScale(b.acc, _step));
		b.pos = DirectX::XMVectorAdd(b.pos, DirectX::XMVectorScale(b.vel, _step));
		b.acc = DirectX::XMVectorZero();
		//Calculating trail
		auto tick = ::GetTickCount();
		if (tick - b.trail.lastTrailTime > 100)
		{
			if (b.trail.trails.size() == 10)
			{
				b.trail.trails.pop_back();
			}
			b.trail.trails.push_front(b.pos);
			b.trail.lastTrailTime = tick;
		}
		//Calculating mass
		totalMass += b.mass;
		//Accumalating Center of Mass
		coM = DirectX::XMVectorAdd(coM, DirectX::XMVectorScale(b.pos, b.mass));
		//Accumalating Center
		center = DirectX::XMVectorAdd(center, b.pos);
	});
	//Setting values
	d.SetTotalMass(totalMass);
	if (totalMass > 0.0f)
		d.SetCoM(DirectX::XMVectorScale(coM, 1.0f / totalMass));
	else
		d.SetCoM(coM);
	if (d.GetTotalBodies() > 0)
		d.SetCenter(DirectX::XMVectorScale(center, 1.0f / d.GetTotalBodies()));
	else
		d.SetCenter(center);
	//Caculating distances
	bool firstDist = true;
	float maxCenterDist = 0.0f;
	float maxCoMDist = 0.0f;
	//Browsing bodies
	d.ForEachBodies([&](Body& b)
	{
		auto distCenter = DirectX::XMVectorGetX(DirectX::XMVector3Length(DirectX::XMVectorSubtract(b.pos, d.GetCenter())));
		auto distCoM = 
			DirectX::XMVectorGetX(
				DirectX::XMVector3Length(
					DirectX::XMVectorSubtract(b.pos, d.GetCoM())));
		//testing first
		if (firstDist)
		{
			maxCenterDist = distCenter;
			maxCoMDist = distCoM;
			firstDist = false;
		}
		else
		{
			maxCenterDist = CLAMP_MIN(maxCenterDist, distCenter);
			maxCoMDist = CLAMP_MIN(maxCoMDist, distCoM);
		}
	});
	d.SetMaxCenterDistance(maxCenterDist);
	d.SetMaxCoMDistance(maxCoMDist);

	return false;
}
