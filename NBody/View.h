#pragma once
#include "Data.h"
#include <Windows.h>
#include "CoordinateConverter.h"

class View
{
private:
	bool				_gravitationalInteraction;
	bool				_trail;
	bool				_clearCanvas;
	float				_maxDistance;
	CoordinateConverter	_cc;
	//Auxilliary
	void ClearCanvas(HDC hdc, int width, int height);
	void DrawAuxilliary(HDC hdc);
	void DrawCenterMarks(HDC hdc, Data& d);
	void DrawLegend(HDC hdc, Data& d, int width, int height);
	void DrawBodies(HDC hdc, Data& d);
public:
	View();
	~View();
	//Initializing/Finalizing
	bool Initialize();
	bool Finalize();
	//Gets
	float GetMaxDistance() { return _maxDistance; }
	//Operations
	bool Render(HDC hdc, int width, int height, Data& d);
	CoordinateConverter& GetCoordinateConverter() { return _cc; }
	//Toggle
	void ToggleTrail() { _trail = !_trail; }
	void ToggleGravitationalInteraction() { _gravitationalInteraction = !_gravitationalInteraction; }
	void ToggleClearCanvas() { _clearCanvas = !_clearCanvas; }
	void IncreaseDistance() { _maxDistance *= 1.1f; }
	void DecreaseDistance() { _maxDistance *= 0.9f; }
	void ResetDistance() { _maxDistance = 0.0f; }
};