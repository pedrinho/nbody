#pragma once
#include <DirectXMath.h>
#include <list>
#include <vector>

template<class eType>
class OldableType
{
private:
	bool _old;
	eType _data;
	eType _initialValue;
public:
	OldableType<eType>(eType initialValue) : _old(false)
	{
		_initialValue = initialValue;
		_data = initialValue;
	}

	eType* operator->()
	{
		if (_old)
		{
			_data = eType();
			_old = false;
		}

		return &_data;
	}

	const eType& GetOldValue()
	{
		_old = true;

		return _data;
	}
};

struct Interaction
{
	DirectX::XMVECTOR	pos;
	float				force;

	Interaction()
	{
		pos = DirectX::XMVectorZero();
		force = 0.0f;
	}
};

struct GravitationalInteraction
{
	OldableType<Interaction>	maxInteraction;

	GravitationalInteraction() :
		maxInteraction(Interaction())
	{
	}
};

struct Trail
{
	std::list<DirectX::XMVECTOR>	trails;
	int								lastTrailTime;

	Trail() : lastTrailTime(0)
	{
	}
};

struct Body
{
	//Attributes
	DirectX::XMVECTOR				pos;
	DirectX::XMVECTOR				vel;
	DirectX::XMVECTOR				acc;
	float							mass;
	bool							deleted;
	Trail							trail;
	GravitationalInteraction		interactions;

	Body() : mass(0.0f), deleted(false), trail()
	{
		pos = DirectX::XMVectorZero();
		vel = DirectX::XMVectorZero();
		acc = DirectX::XMVectorZero();
	}
};

typedef std::vector<Body>	BODIES;