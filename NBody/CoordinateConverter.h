#pragma once
#include <Windows.h>
#include <DirectXMath.h>

class CoordinateConverter
{
private:
	POINT				_viewCenter;
	DirectX::XMVECTOR	_worldCenter;
	float				_toViewFactor;
	float				_toWorldFactor;
	//Auxilliary
	int ConverterToView(float worldPoint, float worldCenter, int viewCenter, bool invert);
	float ConverterToWorld(int viewPoint, int viewCenter, float worldCenter, bool invert);
public:
	CoordinateConverter();
	//Gets and Sets
	POINT&				GetViewCenter() { return _viewCenter; }
	DirectX::XMVECTOR&	GetWorldCenter()	{ return _worldCenter; }
	//Operations
	void Update(POINT& viewCenter, DirectX::XMVECTOR& worldCenter, float maxWorldDistance);
	int ConvertLengthToView(float length);
	void ConvertWorldToView(const DirectX::XMVECTOR& v, POINT& p);
	void ConvertViewToWorld(const POINT& p, DirectX::XMVECTOR& v);
};