#pragma once
#include <functional>
#include "DataTypes.h"
#include "CoordinateConverter.h"
#include "Calculator.h"

class Data
{
private:
	BODIES				_bodies;
	DirectX::XMVECTOR	_coM;
	DirectX::XMVECTOR	_center;
	float				_maxCenterDistance;
	float				_maxCoMDistance;
	int					_trailLenght;
	float				_totalMass;
	//Auxilliary
	float GetMassiveBodyMass();
	bool GetBody(std::function<void(Body&)> f, std::function<bool(Body&, Body&)> p);
	bool GetGreaterMassBody(std::function<void(Body&)> f);
	bool GetLesserMassBody(std::function<void(Body&)> f);
	void CreateNewBody(float x, float y, float m, bool cw);
public:
	//Constructor/Destructor
	Data();
	~Data();
	//Initializing/Finalizing
	bool Initialize();
	bool Finalize();
	//Gets and sets
	const DirectX::XMVECTOR& GetCoM() { return _coM; }
	void SetCoM(DirectX::XMVECTOR& coM) { _coM = coM; }
	const DirectX::XMVECTOR& GetCenter() { return _center; }
	void SetCenter(DirectX::XMVECTOR& center) { _center = center; }
	const float GetMaxCenterDistance() { return _maxCenterDistance; }
	void SetMaxCenterDistance(const float d) { _maxCenterDistance = d; }
	const float GetMaxCoMDistance() { return _maxCoMDistance; }
	void SetMaxCoMDistance(const float d) { _maxCoMDistance = d; }
	int GetTotalBodies();
	int GetTrailLenght() { return _trailLenght; }
	float GetTotalMass() { return _totalMass; }
	void SetTotalMass(float m) { _totalMass = m; }
	void GetHeaviestBodyPos(DirectX::XMVECTOR& pos);
	//Interacting bodies
	bool ForEachBodies(std::function<void(Body&)> f);
	bool InteractBodies(std::function<void(Body&, Body&)> f);
	//Toggle
	void KillVelocity();
	void IncreaseFX();
	void DecreaseFX();
	void CreateNewBody(int mode, bool cw, float maxDistance);
	void CreateNewBody(POINT p, CoordinateConverter& cc, bool massive, bool cw);
	void DeleteBody();
	void ToggleA();
};