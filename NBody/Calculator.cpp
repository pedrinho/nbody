#include "stdafx.h"
#include "Calculator.h"

float Calculator::GetRadius(float mass)
{
	return (mass > 0.0f ? 1.0f*::logf(mass) : 0.0f);
}

DirectX::XMVECTOR Calculator::GetForce(Body & b1, Body & b2)
{
	auto sub0 = DirectX::XMVectorSubtract(b2.pos, b1.pos);
	auto dist0 = DirectX::XMVectorGetX(DirectX::XMVector2Length(sub0));
	auto subNormal = DirectX::XMVector2Normalize(sub0);
	auto mass1 = CLAMP_MIN(b1.mass, 1.0f);
	auto mass2 = CLAMP_MIN(b2.mass, 1.0f);

	return DirectX::XMVectorScale(subNormal, (mass1 * mass2) / (dist0 * dist0));
}

void Calculator::CalculateOrbit(Body & center, Body & newBody, float x, float y, float mass, float cw)
{
	auto np = DirectX::XMVectorSet(x, y, 0.0f, 0.0f);
	newBody.mass = mass;
	newBody.pos = np;
	auto sub = DirectX::XMVectorSubtract(center.pos, newBody.pos);
	auto vNorm = DirectX::XMVector2Normalize(DirectX::XMVector2Orthogonal(sub));
	auto d = DirectX::XMVectorGetX(DirectX::XMVector2Length(sub));
	auto nv = DirectX::XMVectorScale(vNorm, (cw ? -1.0f : 1.0f)*::sqrtf(center.mass / ::fabsf(d)));
	newBody.vel = DirectX::XMVectorAdd(nv, center.vel);
}
