#include "stdafx.h"
#include "Data.h"
#include <algorithm>
#include <random>
#include <iostream>

Data::Data() : _maxCenterDistance(0.0f), _maxCoMDistance(0.0f), _trailLenght(10)
{
	_coM = DirectX::XMVectorZero();
	_center = DirectX::XMVectorZero();
}

Data::~Data()
{
}

bool Data::Initialize()
{
	std::random_device rd;
	std::mt19937 gen(rd());
	/*_bodies.resize(200);
	//Creating bodies
	ForEachBodies([&gen](Body& b)
	{
		b.pos = DirectX::XMVectorSet(
			10000.0f*(-0.33f + 0.66f*std::generate_canonical<float, 10>(gen)),
			10000.0f*(-0.33f + 0.66f*std::generate_canonical<float, 10>(gen)),
			0.0f,
			0.0f);
		b.mass = 5.0f + 100.0f*std::generate_canonical<float, 10>(gen);
	});*/

	Body c;
	c.mass = 1E+9f;
	c.pos = DirectX::XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);;
	c.vel = DirectX::XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
	_bodies.push_back(c);
	/*c.mass = 0.0f;
	c.pos = DirectX::XMVectorSet(0.0f, 100.0f, 0.0f, 0.0f);;
	_bodies.push_back(c);*/
	/*Body b;
	/*for (int i = 0; i < 100; i++)
	{
		lCalculateOrbit(
			c,
			b,
			10000.0f*(-0.33f + 0.66f*std::generate_canonical<float, 10>(gen)),
			10000.0f*(-0.33f + 0.66f*std::generate_canonical<float, 10>(gen)),
			0.0f*(0.5f + 0.5f*std::generate_canonical<float, 10>(gen)));
		_bodies.push_back(b);
	}*/
	/*Calculator::CalculateOrbit(
		c,
		b,
		1000.0f,
		0.0f,
		1E+8f,
		true);
	_bodies.push_back(b);*/

	/*Body a;
	Calculator::CalculateOrbit(
		b,
		a,
		100.0f,
		0.0f,
		0.0f,
		true);
	_bodies.push_back(a);*/


	/*c.mass = 0.0f;
	c.pos = DirectX::XMVectorSet(0.0f, -1000.0f, 0.0f, 0.0f);
	c.vel = DirectX::XMVectorSet(0.0f, 400.0f, 0.0f, 0.0f);
	_bodies.push_back(c);*/

	//***************************************************
	/*Body c;
	c.mass = 1E+7f;
	c.pos = DirectX::XMVectorZero();
	_bodies.push_back(c);
	Body b;
	for (int i = 4; i < 10; i++)
	{
		lCalculateOrbit(c, b, 0.0f, 100.0f*i, 0.0f);
		_bodies.push_back(b);
	}*/

	//*********************************************
	/*Body a;
	a.mass = 0.0f;
	a.pos = DirectX::XMVectorSet(5.0f, 0.5f, 0.0f, 0.0f);
	a.vel = DirectX::XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
	_bodies.push_back(a);
	a.mass = 0.0f;
	a.pos = DirectX::XMVectorSet(3.0f, -1.0f, 0.0f, 0.0f);
	a.vel = DirectX::XMVectorSet(1.0f, 1.0f, 0.0f, 0.0f);
	_bodies.push_back(a);*/

	return false;
}

bool Data::Finalize()
{
	_bodies.clear();

	return false;
}

void Data::GetHeaviestBodyPos(DirectX::XMVECTOR& pos)
{
	GetGreaterMassBody([&pos](Body& b)
	{
		pos = b.pos;
	});
}

bool Data::ForEachBodies(std::function<void(Body&)> f)
{
	auto it = std::remove_if(_bodies.begin(), _bodies.end(), [f](Body& b)
	{
		if (!b.deleted)
		{
			f(b);
		}

		return b.deleted;
	});
	if (it != _bodies.end())
	{
		_bodies.erase(it, _bodies.end());
	}

	return false;
}

bool Data::InteractBodies(std::function<void(Body&, Body&)> f)
{
	if (_bodies.size() > 0)
	{
		for (auto i = _bodies.begin(); i != _bodies.end() - 1; i++)
		{
			auto& b1 = *i;
			if (!b1.deleted)
			{
				for (auto j = i + 1; j < _bodies.end(); j++)
				{
					auto& b2 = *j;
					if (!b2.deleted)
					{
						f(b1, b2);
					}
				}
			}
		}
	}

	return false;
}

bool Data::GetBody(std::function<void(Body&)> f, std::function<bool(Body&, Body&)> p)
{
	auto ret = false;
	auto it = std::max_element(_bodies.begin(), _bodies.end(), [&p](Body& b1, Body& b2)
	{
		return p(b1, b2);
	});

	if (it != _bodies.end())
	{
		auto& b = *it;
		f(b);
		ret = true;
	}

	return ret;
}

bool Data::GetGreaterMassBody(std::function<void(Body&)> f)
{
	return GetBody(f, [](Body& b1, Body& b2)
	{
		return (!b1.deleted && !b2.deleted && b1.mass < b2.mass);
	});
}

bool Data::GetLesserMassBody(std::function<void(Body&)> f)
{
	return GetBody(f, [](Body& b1, Body& b2)
	{
		return (!b1.deleted && !b2.deleted && b1.mass > b2.mass);
	});
}

void Data::KillVelocity()
{
	ForEachBodies([](Body& b)
	{
		b.vel = DirectX::XMVectorZero();
	});
}

void Data::IncreaseFX()
{
	ForEachBodies([](Body& b)
	{
		b.mass *= 1.1f;
	});
}

void Data::DecreaseFX()
{
	ForEachBodies([](Body& b)
	{
		b.mass *= 0.9f;
	});
}

void Data::CreateNewBody(float x, float y, float m, bool cw)
{
	Body nb;
	nb.pos = DirectX::XMVectorSet(x, y, 0.0f, 0.0f);
	nb.mass = m;
	GetBody([&](Body& b)
	{
		Calculator::CalculateOrbit(b, nb, x, y, m, cw);
	}, [&nb](Body& b1, Body& b2)
	{
		auto f1 = DirectX::XMVectorGetX(DirectX::XMVector2Length(Calculator::GetForce(nb, b1)));
		auto f2 = DirectX::XMVectorGetX(DirectX::XMVector2Length(Calculator::GetForce(nb, b2)));

		return f1 < f2;
	});
	_bodies.push_back(nb);
}

void Data::CreateNewBody(int mode, bool cw, float maxDistance)
{
	//Generating random pos
	std::random_device rd;
	std::mt19937 gen(rd());
	auto pos = DirectX::XMVectorSet(
		maxDistance*(-0.8f + 1.6f*std::generate_canonical<float, 10>(gen)),
		maxDistance*(-0.8f + 1.6f*std::generate_canonical<float, 10>(gen)),
		0.0f,
		0.0f);
	auto mass = (mode == 0 ? 0.0f : GetMassiveBodyMass());
	//Getting massive body
	Body nb;
	Body* c;
	if (GetGreaterMassBody([&c](Body& b)
	{
		c = &b;
	}))
	{
		pos = DirectX::XMVectorAdd(pos, c->pos);
		Calculator::CalculateOrbit(
			*c,
			nb,
			DirectX::XMVectorGetX(pos),
			DirectX::XMVectorGetY(pos),
			mass,
			cw);
	}
	else
	{
		nb.pos = pos;
		nb.mass = mass;
	}
	_bodies.push_back(nb);
}

float Data::GetMassiveBodyMass()
{
	return CLAMP_MIN(0.001f*_totalMass, 1E+6f);
}

void Data::CreateNewBody(POINT p, CoordinateConverter& cc, bool massive, bool cw)
{
	DirectX::XMVECTOR v;
	cc.ConvertViewToWorld(p, v);
	CreateNewBody(DirectX::XMVectorGetX(v), DirectX::XMVectorGetY(v), (massive ? GetMassiveBodyMass() : 0.0f), cw);
}

void Data::DeleteBody()
{
	/*auto size = std::count_if(_bodies.begin(), _bodies.end(), [](Body& b)
	{
		return !b.deleted;
	});*/

	if(_bodies.size() > 0)
		_bodies.pop_back();
	//if (size > 1)
	/*{
		GetLesserMassBody([](Body& b)
		{
			b.deleted = true;
		});
	}*/
}

int Data::GetTotalBodies()
{
	return (int)_bodies.size();
}

void Data::ToggleA()
{
	float radius = 0.0f;
	float mass = 0.0f;
	DirectX::XMVECTOR pos = DirectX::XMVectorZero();
	//Getting greater mass body
	if (GetGreaterMassBody([&](Body& b)
	{
		mass = b.mass;
		pos = b.pos;
		radius = CLAMP_MIN(Calculator::GetRadius(b.mass), 1.0f);
	}))
	{
		auto d = 50.0f*radius;
		Body nb;
		nb.mass = 0.1f*mass;
		nb.pos = DirectX::XMVectorAdd(pos, DirectX::XMVectorSet(0.0f, -d, 0.0f, 0.0f));
		nb.vel = DirectX::XMVectorSet(0.0f, 0.1f*d, 0.0f, 0.0f);
		_bodies.push_back(nb);

		nb.pos = DirectX::XMVectorAdd(pos, DirectX::XMVectorSet(0.0f, d, 0.0f, 0.0f));
		nb.vel = DirectX::XMVectorSet(0.0f, -0.1f*d, 0.0f, 0.0f);
		_bodies.push_back(nb);
	}
}